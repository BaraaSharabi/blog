import pytest
from blog.mainBlog import create_app


@pytest.fixture
def client():
    app = create_app("TESTING")
    flask = app.app
    client = flask.test_client()
    return client


def test_home_page_get(client):
    """
    Check that the response is valid
    When (GET) request sent to a page '/' 
    """

    response = client.get('/')
    assert response.status_code == 200
    assert "!q to exit team's Blog" in response.data.decode('utf-8')
    assert "Read" in response.data.decode('utf-8')
    assert "Home Page" in response.data.decode('utf-8')
    assert "Contact Us" in response.data.decode('utf-8')
    assert "About Us" in response.data.decode('utf-8')


def test_home_page_post(client):
    """
    Check that  a '405' status code is returned 
    When (POST) request sent to a page '/' 
    """

    response = client.post('/')
    assert response.status_code == 405
    assert b"!q to exit team's Blog" not in response.data


def test_find_the_post(client):
    """
    Check find_the_post helper method  
    """

    response = client.get('/')
    app = create_app("TESTING")
    data = app.get_posts_data()
    the_post = app.find_the_post(data, 1)
    assert str(the_post['title']) in response.data.decode('utf-8')
    assert str(the_post['body']) not in response.data.decode('utf-8')


def test_about_page_get(client):
    """
    Check that the response is valid
    When (GET) request sent to a page '/about' 
    """

    response = client.get('/about')

    assert response.status_code == 200
    assert "You can use the application from any device like mobile phone , tablet , or desktop." in response.data.decode(
        'utf-8')
    assert 'is a free social networking RESTful Blog Web Application.' in response.data.decode(
        'utf-8')
    assert "About Us" in response.data.decode('utf-8')


def test_about_page_post(client):
    """
    Check that  a '405' status code is returned 
    When (POST) request sent to a page '/' 
    """

    response = client.post('/about')
    assert response.status_code == 405
    assert b"!q to exit team's Blog" not in response.data


def test_about_page_get(client):
    """
    Check that the response is valid
    When (GET) request sent to a page '/contact'
    """

    response = client.get('/contact')

    assert response.status_code == 200
    assert "Fill out the form below if you need any help!" in response.data.decode(
        'utf-8')
    assert "Name" in response.data.decode('utf-8')
    assert "Feedback" in response.data.decode('utf-8')
    assert "Email Address" in response.data.decode('utf-8')


def test_post_page_get(client):
    """
    Check that the response is valid
    When (GET) request sent to a page '/blog_post/{post_id}' 
    """

    app = create_app("TESTING")
    for i in range(len(app.get_posts_data())):

        response = client.get('/blog_post/'+str(i))
        assert response.status_code == 200


def test_post_page_get(client):
    """
    Check that  a '405' status code is returned 
    When (POST) request sent to a page '//blog_post/{post_id}' 
    """

    app = create_app("TESTING")
    for i in range(len(app.get_posts_data())):

        response = client.post('/blog_post/'+str(i))
        assert response.status_code == 405
