from blog.mainBlog import *
import argparse


app = create_app(__name__).app


if __name__ == "__main__":
    #app.run(debug=True)
    ap = argparse.ArgumentParser()
    ap.add_argument("-I", "--ip", type=str, default="localhost")
    ap.add_argument("-P", "--port", type=int, default=8080)
    ap.add_argument("-D", "--debug", type=bool, default=True)
    args = vars(ap.parse_args())

    app.run(host=args["ip"], port=args["port"], debug=args["debug"])
