import os
import click
from flask.cli import with_appcontext


HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
PROFILER_PATH = os.path.join(PROJECT_ROOT, 'profiler')


@click.command(name='profile')
@with_appcontext
def profile():
    import cProfile
    import pstats
    import subprocess
    from blog import mainBlog


    if not os.path.exists(PROFILER_PATH):
        os.mkdir(PROFILER_PATH)

    prof = cProfile.Profile()

    print("\033[1;32mProfiling started")

    prof.runctx('mainBlog.create_app(__name__)', globals(), locals())
    prof.dump_stats(PROFILER_PATH + '/output.prof')

    print("\033[1;32mPreparing output")

    stream = open(PROFILER_PATH + '/output.txt', 'w')
    stats = pstats.Stats(PROFILER_PATH + '/output.prof', stream=stream)
    stats.sort_stats('cumtime')
    stats.print_stats()

    subprocess.run(["py-spy", "record", "-o",
                   "profiler/profile.svg", "--", "python", "app.py"])
   
    print("\033[1;32mProfiling finished")

