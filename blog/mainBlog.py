from flask import Flask, render_template, request
import requests
from blog import profiler, test


class Bolgs:
    def __init__(self, name="Blog", *args, **kw):
        self.app = Flask(name, *args, **kw)
        self.app.cli.add_command(profiler.profile)
        self.app.cli.add_command(test.test)
        self.route_urls()


    def run(self, host=None, port=None, debug=None):
        self.app.run(host=host, port=port, debug=debug, use_reloader=False)


    def get_posts_data(self):
        data_url = "https://api.npoint.io/af07d0c0049167c86df1"
        posts_data = requests.get(data_url).json()
        return posts_data


    def find_the_post(self, data, id):
        for post in data:
            if post['id'] == id:
                return post


    def route_urls(self):
        @self.app.route("/")
        def home():
            data = self.get_posts_data()
            return render_template("index.html", posts_data=data)


        @self.app.route("/blog_post/<int:post_id>")
        def post(post_id):
            data = self.get_posts_data()
            the_post = self.find_the_post(data, post_id)
            return render_template("post.html", posts_data=data, id=post_id, post=the_post)


        @self.app.route("/about")
        def about():
            data = self.get_posts_data()
            return render_template("about.html", posts_data=data)


        @self.app.route("/contact", methods=["GET", "POST"])
        def contact():
            if request.method == "POST":
                data = request.form
                # print(data["name"])
                # print(data["email"])
                # print(data["message"])
                #get_feedback(data["name"], data["email"], data["message"])
                return render_template("contact.html", send_successfully=True)
            return render_template("contact.html", send_successfully=False)


def create_app(name):
    return Bolgs(name, template_folder="blog/templates", static_folder="blog/static")

