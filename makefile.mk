
## 🏃 Run the server locally using Python & Flask
run: 
	python -m pip install -r requirements.txt
	export FLASK_APP=path/to/mainBlog.py	 
	flask run --no-reload

## 🎯 Unit tests for Flask app
test:
	pip install -r requirements.txt
	flask test
