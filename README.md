
# Flask Application

This is a free social networking Restful Blog Web Application in which the user is able to sign up for a new account, post articals, pictures and short posts. Users can use thier own mobile phone or PC to use this app.



## Prerequisites 
- python 3.9
- pip



### Install dependencies
May need admin rights
```bash
python -m pip install -r requirements.txt
```



## Local Deployment
```bash
flask run --no-reload
```
Go To https://127.0.0.1:5000



## Running Tests

To run tests, run the following command
Run the server locally using Python & Flask:
```bash
    python -m pip install -r requirements.txt
	export FLASK_APP=path/to/mainBlog.py	 
	flask run --no-reload
```
Unit tests for Flask app:
```bash
    pip install -r requirements.txt
 flask test
```

### Running the profiler
```bash
pip install py-spy

flask profile
```



## Troubleshooting 
If you encounters any problem when running the application using

```bash
flask run --no-reload
```
try setting the enviroment variables as shown under "Envieroment Variables"


## Environment Variables

You might need to set the enviroment variable

Using a bash terminal, execute
```bash
export FLASK_APP=path/to/mainBlog.py
```

if you are using powershell terminal, execute 
```powershell
$env:FLASK_APP =path/to/main.py
```


## Authors
- [Ebrahim Al-Shargabi](https://gitlab.com/Ebo_12)
- [Rustam Temirov](https://gitlab.com/rustikjon99)
- [Al-Baraa Al-Sharabi](https://gitlab.com/BaraaSharabi)



## License
MIT License



Copyright (C) 2022 :!q to exit team



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:



The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
