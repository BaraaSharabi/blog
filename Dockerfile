FROM python:3.9-slim-buster

LABEL Name="Restful Blog Web App"
LABEL org.opencontainers.image.source = "https://gitlab.com/Ebo_12/restful-blog-web-app"

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY blog ./blog
COPY app.py .

CMD flask run --no-reload