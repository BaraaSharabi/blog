Flask>=2.2.2
Jinja2>=3.0
pytest>=7.1.3
markupsafe>=2.0.1
requests==2.28.1